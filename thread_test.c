#include "types.h"
#include "stat.h"
#include "user.h"
#include "thread.h"

#define THREADS_CNT 10

int cnt = 0;

void * fn(void *arg) {
  ++cnt;
  printf(1, "fn: %d\n", cnt);
  return (void *)cnt;
}

int 
main(int argc, char ** argv) {
  struct thread threads[THREADS_CNT];
  int ret, i;

  for (i = 0; i != THREADS_CNT; ++i) {
    thread_create(&threads[i], fn, 0);
  }

  for (i = 0; i != THREADS_CNT; ++i) {
    thread_join(&threads[i], (void**)&ret);
    printf(1, "ret: %d\n", ret);
  }

  exit();
}
