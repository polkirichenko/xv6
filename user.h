#define MUTEX_CNT 100

struct stat;
struct rtcdate;
//struct thread;

// system calls
int fork(void);
int exit(void) __attribute__((noreturn));
int wait(void);
int pipe(int*);
int write(int, void*, int);
int read(int, void*, int);
int close(int);
int kill(int);
int exec(char*, char**);
int open(char*, int);
int mknod(char*, short, short);
int unlink(char*);
int fstat(int fd, struct stat*);
int link(char*, char*);
int mkdir(char*);
int chdir(char*);
int dup(int);
int getpid(void);
char* sbrk(int);
int sleep(int);
int uptime(void);
int mkfifo(char*);
int chuid(int);
int chmod(char*, int);
int clone(char*, int);
int sched_yield(void);
int lock(int);
int unlock(int);
int save(int);
int load(void);

// ulib.c
int stat(char*, struct stat*);
char* strcpy(char*, char*);
void *memmove(void*, void*, int);
char* strchr(const char*, char c);
int strcmp(const char*, const char*);
void printf(int, char*, ...);
char* gets(char*, int max);
uint strlen(char*);
void* memset(void*, int, uint);
void* malloc(uint);
void free(void*);
int atoi(const char*);

// users_fun.c
/*
char * readall(int * fd);
int getuid(char * buf);
void rev(char * p);
char * itoa(int x);
void strcat(char * dst, char * scr);
char * getname(char ** p, char stop);
int search_log(char * login, char * buf, char ** pass, int * uid);
int check_passwd(char ** buf, char * login);
*/

// thread.c
//int thread_create(struct thread *, void *(*start_routine)(void *), void *arg);
//int thread_join(struct thread *, void **);
