#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "users_fun.h"

char *argv[] = { "sh", 0 };

int login(char * login) {
    int fd = 0;
    char * buf = readall(&fd);
    close(fd);
    int check = check_passwd(&buf, login);
    if (check == -1)
        return -1;

    if (check) {
        int uid;
        char p[MAXLEN_PASSWD];
        char * pass = p;
        search_log(login, buf, &pass, &uid);
        chuid(uid);
        exec("sh", argv);
    } else {
        printf(1, "wrong password\n");
    }

    return 0;
}

int
main()
{
    char un[MAXLEN_LOGIN];
    printf(1, "username: ");
    int length = read(0, un, MAXLEN_LOGIN);
    *(un + length - 1) = '\0';
    if (login(un) < 0)
        printf(2, "failed to login as %s\n", un);

    exit();
}