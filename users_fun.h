#ifndef USERS_FUN
#define USERS_FUN

#define MAXLEN_LOGIN 20
#define MAXLEN_PASSWD 20
#define max(a, b) ((a) > (b) ? (a) : (b))

char * readall(int * fd);
int getuid(char * buf);
void rev(char * p);
char * itoa(int x);
void strcat(char * dst, char * scr);
char * getname(char ** p, char stop);
int search_log(char * login, char * buf, char ** pass, int * uid);
int check_passwd(char ** buf, char * login);

char * readall(int * fd) {
    *fd = open("passwd_info", O_RDONLY);
    struct stat st;
    stat("passwd_info", &st);
    char * buf = (char *)malloc(st.size + 1);
    int len = read(*fd, buf, st.size);
    *(buf + len) = '\0';
    return buf;
}

int getuid(char * buf) {
    int max = 0;
    int count = 0;
    char u[3];
    char * uid = u;
    while(strcmp(getname(&buf, ':'), "\0") != 0) {
        ++count;
        getname(&buf, ':');
        uid = getname(&buf, '\n');
        if (atoi(uid) > max)
            max = atoi(uid);
    }
    if (count >= 100)
        return -1;
    else
        return max + 1;
}

void rev(char * p)
{
    char * q = p;
    while (*q) 
        ++q;
    for(--q; p < q; ++p, --q) {
        *p = *p ^ *q;
        *q = *p ^ *q;
        *p = *p ^ *q;
    }
}

char * itoa (int x) {
    char res[3]; // depends on the max number of users, here 100
    char * st = res;
    if (x == 0) {
        res[0] = '0';
        res[1] = '\0';
    } else {
        while (x) {
            *st++ = '0' + x % 10;
            x /= 10;
        }
        *st = '\0';
        rev(res);
    }
    char * r = res;
    return r;
}

void strcat(char * dst, char * scr) {
    char * p = dst;
    for (; *p != '\0'; ++p);
    char * s = scr;
    while (*s != '\0')
        *p++ = *s++;
    *p = '\0';
}

char * getname(char ** p, char stop) {
    char * r = (char * )malloc(max(MAXLEN_LOGIN, MAXLEN_PASSWD) + 1);
    char * res = r;
    if (**p == '\0') {
        *res = **p;
        return r;
    }
    while (**p != stop) {
        *res = **p;
        ++res; ++(*p);
    }
    ++(*p);
    *res = '\0';
    return r;
}

int search_log(char * login, char * buf, char ** pass, int * uid) {
    char * p = buf;    
    while (*p != '\0') {
        char * l = getname(&p, ':');
        if (strcmp(l, login) == 0) {
            strcpy(*pass, getname(&p, ':'));
            *uid = atoi(getname(&p, '\n'));
            return 0;
        } else {
            for (; *p != '\n'; ++p);
            ++p;
        }
    }
    return -1;
}

int check_passwd(char ** buf, char * login) {
    char p[MAXLEN_PASSWD];
    char * pass = p;
    int uid;
    if (search_log(login, *buf, &pass, &uid) == -1)
        return -1;

    printf(1, "password for %s: ", login);
    char user_passwd[MAXLEN_PASSWD];
    int length = read(0, user_passwd, MAXLEN_PASSWD);
    *(user_passwd + length - 1) = '\0';
    return (strcmp(user_passwd, pass) == 0);
}

#endif // USERS_FUN