#xv6

xv6 is a teaching operating system developed for MIT's Operating Systems course. This project was a part of Operating System course at Higher School of Economics and was focused on enlarging the system with extra functionality.

###1. exec
Added interpreted scripts execution: when there is a line starting with "#!" in the beginning of the file, we retrieve a path to interpreter, add it to the list of arguments and call `exec` again (using new arguments).
Maximum recursion depth -- 5.

###2. mkfifo
As named pipes are part of the file system, a new inode type T_NPIPE was created for them. New fields -- rend and wend are added to inode struct, which are pointers to file structs, pipe's ends for reads and writes respectively. `mkfifo`
<named pipe name> creates T_NPIPE inode with nullpointers rend and wend. When pipe is opened for the first time, `pipealloc` is called (pipe.c) with rend and wend arguments. The memory for struct pipe is allocated and `filealloc` is called for pipe ends (they have special FD_NPIPE type). Depending on in which mode the pipe was opened (O_RDONLY or O_WRONLY), `open` will return a file descriptor of the corresponding pipe end. When named pipe is opened for read or write, the process will sleep until it is opened from the different end. When it is opened with flag O_NONBLOCK set, the process will not sleep (i.e., for pipe to be showed by `ls` command).

###3. users
Added functions:

* `login` (called in `init`, asks for login and password, changes process uid and calls `sh`),

* `useradd` (adds new user, writes "<login>::<uid>" to passwd_info file without initial password, uid is assigned as max existing uid + 1),

* `passwd` (changes user password),

* `userdel` (deletes the user, except you cannot delete root).

passwd_info file stores information about users (it always has a line "root:<password>:0"), it is owned by super user root. Format in passwd_info is "login:password:uid".

Added fields uid and euid (effective uid) to struct `proc`, processes are launched by users. Added fields uid and perm (user id and permissions) to struct dinode, inode and stat. File permissions are stored as srwxrwx: suid-bit, 3 bits of owners permissions, 3 bits of other users permissions (root has any access to any file). Added sys call `chuid` which changes process uid, sys call `chmod` changes file permissions (can be used by root or file owner). Added permission checks to sys calls `open`, `unlink`, `create`, `chdir`.

If suid-bit is set in file permissions, while executing it, euid of the process will be changed to file owner's uid (suid-bit is set in passwd so that users would be able to change their password, otherwise only root has write access to passwd_info).
`ls` shows owner's uid and permissions.


###4. threads
Added sys call `clone` which resembles `fork` but after copying the process there are 2 processes with common memory (each process has its own stack). Struct thread contains main information about thread. Added new fields in `proc` for threads: pointers to user-stack, pointer to struct thread, realsz and sz -- memory size of parent and pointer to this size in all child threads (for memory synchronization between threads).
`thread_create` calls `clone` and creates new thread. Thread local variables become mutual after consecutive clone calls, as the following call overwrites local variables and function arguments. In order to fix this issue, syscalls saving and loading thread data were added.
`thread_join` waits for thread termination and clears the stack. `exit` for thread does not wake parent up, and parent kills all child processes.

###5. mutex
Process can lock the mutex (which is a number from 0 to MUTEX_CNT). Locked mutex has owner process and only this process can unlock it. If some process tries to lock the mutex while it is already locked, the process gets blocked
and becomes one of the processes awaiting for this mutex's release. When the mutex is released, all these awaiting processes are awakened. 