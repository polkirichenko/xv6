#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "users_fun.h"

void change_pass(int fd, char * login, char ** buf, char * newpass) {
    char * p = *buf;
    int size = strlen(*buf) + strlen(newpass) + 1;
    while (*p != '\0') {
        char * l = getname(&p, ':');
        if (strcmp(l, login) == 0) {
            char * rest;
            if (*p == ':') {
                *p = '\0';
            } else {
                *p = '\0';
                for (; *p != ':'; ++p);
            }
            rest = ++p;

            char start[size];
            strcpy(start, *buf);
            strcat(start, newpass);
            strcat(start, ":");
            strcat(start, rest);

            write(fd, start, strlen(start));
            return;
        } else {
            for (; *p != '\n'; ++p);
            ++p;
        }
    }
}

int passwd(char * login) {
    int fd = 0;
    char * buf = readall(&fd);
    close(fd);
    int check = check_passwd(&buf, login);
    if (check == -1)
        return -1;

    if (check) {
        printf(1, "enter new password: ");
        char newpass[MAXLEN_PASSWD];
        int l1 = read(0, newpass, MAXLEN_PASSWD);
        *(newpass + l1 - 1) = '\0';
        printf(1, "retype new password: ");
        char retype[MAXLEN_PASSWD];
        int l2 = read(0, retype, MAXLEN_PASSWD);
        *(retype + l2 - 1) = '\0';
        if (strcmp(newpass, retype) == 0) {
            fd = open("passwd_info", O_WRONLY | O_TRUNC);
            change_pass(fd, login, &buf, newpass);
            close(fd);
        }
        else
            printf(1, "wrong retype\n");
    } else {
        printf(1, "wrong password\n");
    }
    return 0;
}

int
main(int argc, char *argv[])
{
    if (argc < 2){
        printf(2, "username not specified\n");
        exit();
    }

    if (passwd(argv[1]) < 0)
      printf(2, "passwd: password for %s failed to be changed\n", argv[1]);

    exit();
}
