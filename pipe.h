#include "spinlock.h"

#define PIPESIZE 512

struct pipe {
  struct spinlock lock;
  char data[PIPESIZE];
  uint nread;     // number of bytes read
  uint nwrite;    // number of bytes written
  int readopen;   // number of processes which opened pipe for reading
  int writeopen;  // number of processes which opened pipe for writing
};
