#include "types.h"
#include "defs.h"
#include "param.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"

#define MUTEX_CNT 100

struct spinlock mutex_lock;
struct proc *mutex_proc[MUTEX_CNT];

void
mutexinit(void)
{
  memset(mutex_proc, 0, sizeof(mutex_proc));
}

int
sys_lock(void)
{
  int id;

  if (argint(0, &id) < 0 || id < 0 || id >= MUTEX_CNT)
    return -1;

  acquire(&mutex_lock);

  while (mutex_proc[id]) {
    if (mutex_proc[id]->killed)
      return -1;
    sleep((void*)id, &mutex_lock);
  }

  mutex_proc[id] = proc;
  release(&mutex_lock);
  return 0;
}

int
sys_unlock(void)
{
  int id;

  if (argint(0, &id) < 0 || id < 0 || id >= MUTEX_CNT)
    return -1;

  acquire(&mutex_lock);
  if (mutex_proc[id] != proc) {
    release(&mutex_lock);
    return -1;
  }

  mutex_proc[id] = 0;
  release(&mutex_lock);
  wakeup((void*)id);

  return 0;
}
