#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "x86.h"

int
main(int argc, char *argv[])
{
    if(argc < 3) {
      printf(2, "too few arguments for chmod\n");
      exit();
    }

    int mode = 0;
    char * p = argv[1];
    while (*p) {
        mode *= 8;
        mode += *p - '0';
        ++p;
    }

    if(chmod(argv[2], mode) < 0)
        printf(2, "chmod: failed to change mode for %s\n", argv[2]);

    exit();
}
