#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "users_fun.h"

void delete(char * login, char ** buf) {
    char * p = *buf;
    while (*p != '\0') {
        char * left = p;
        char * l = getname(&p, ':');
        if (strcmp(l, login) == 0) {
            for (; *p != '\n'; ++p);
            char * right = ++p;
            while (*right != '\0') {
                *left++ = *right++;
            }
            *left = '\0';
            return;
        } else {
            for (; *p != '\n'; ++p);
            ++p;
        }
    }
}

int
userdel(char * login) {
    if (strcmp(login, "root") == 0) {
        printf(1, "error: you cannot delete root\n");
        return -1;
    }
    int fd = 0;
    char * buf = readall(&fd);
    close(fd);
    int check = check_passwd(&buf, login);
    if (check == -1)
        return -1;

    if (check) {
        delete(login, &buf);
        fd = open("passwd_info", O_WRONLY | O_TRUNC);
        write(fd, buf, strlen(buf));
        close(fd);
    } else {
        printf(1, "wrong password\n");
    }

    return 0;
}

int
main(int argc, char *argv[])
{
    if (argc < 2){
        printf(2, "username not specified\n");
        exit();
    }

    if (userdel(argv[1]) < 0)
        printf(2, "failed to delete user %s\n", argv[1]);

    exit();
}
