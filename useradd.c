#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "x86.h"
#include "users_fun.h"

int useradd(char * login) {
    int fd = 0;
    char * buf = readall(&fd);
    close(fd);
    
    char upd_passwd[strlen(buf) + strlen(login) + 6];
    strcpy(upd_passwd, buf);
    strcat(upd_passwd, login);
    strcat(upd_passwd, "::");
    strcat(upd_passwd, itoa(getuid(buf)));
    strcat(upd_passwd, "\n");

    fd = open("passwd_info", O_WRONLY);
    write(fd, upd_passwd, strlen(upd_passwd));
    close(fd);
    return 0;
}

int
main(int argc, char *argv[])
{
    if(argc < 2) {
      printf(2, "username not specified\n");
      exit();
    }

    if(useradd(argv[1]) < 0)
        printf(2, "useradd: %s failed to create\n", argv[1]);

    exit();
}
