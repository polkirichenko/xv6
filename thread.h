#ifndef THREAD_H
#define THREAD_H

struct thread {
  int pid;
  char *ustack;
  char init, exited;
  void *(*start_routine)(void*);
  void *arg;
  void *ret;
};

void
thread_start(struct thread *thread)
{
  thread->exited = 0;
  thread->init = 0;
  thread->ret = (thread->start_routine)(thread->arg);
  thread->exited = 1;
}

int
thread_create(struct thread *thread, void *(*start_routine)(void *), void *arg) {
  char *ustack;
  int pid;

  thread->init = 1;

  if (!(ustack = malloc(4096))) {
    thread->init = 0;
    return -1;
  }

  save((int)thread);
  thread->ustack = ustack;
  thread->start_routine = start_routine;
  thread->arg = arg;
 
  if ((pid = clone(ustack, 4096)) < 0) {
    free(thread->ustack);
    thread->init = 0;
    return -1;
  }

  if (pid) {
    thread->pid = pid;
    while (thread->init)
      sched_yield();

    return 0;
  }

  thread_start((struct thread *)load());  
  exit();
}

int
thread_join(struct thread *thread, void **retval) {
  while (!thread->exited) {
    sched_yield();
  }

  free(thread->ustack);
  if (retval) {
    *retval = thread->ret;
  }

  return 0;
}

#endif // THREAD_H